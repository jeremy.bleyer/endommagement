# UE `Endommagement`/`Damage mechanics`

### Master M2 Mécanique des Solides : Matériaux et Structures (Sorbonne Université, Ecole des Ponts ParisTech)

### Responsables: Kim Pham (kim.pham@ensta-paris.fr), Jérémy Bleyer (jeremy.bleyer@enpc.fr)

### Repository

Ths repository contains FEniCS and FEniCSx scripts used for both numerical work sessions:

* **TD1** (17/01/2023): *Numerical aspects of local damage models*
* **TD2** (31/01/2023): *Numerical aspects of gradient damage models*

### Dependencies

- legacy FEniCS:
    * `FEniCS` (versions 2018/2019)
    * `mshr`
or
- new FEniCSx:
    * `FEniCSx` (version 0.5.1)
    * `gmsh`
    * `pyvista` for plotting results
 optionally `Paraview` for better visualization of the results

### Online use with Binder

In case of installation trouble with FEniCS(x), it is possible to run scripts online with Binder.
Note that computing times are probably larger due to limited performances of the offered cloud computing solution by Binder.

**Click on the badge below** and wait a few moments for the image to launch:

- legacy FEniCS:
 [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.enpc.fr%2Fnavier-fenics%2Ffenics-binder/HEAD?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.enpc.fr%252Fjeremy.bleyer%252Fendommagement%26urlpath%3Dlab%252Ftree%252Fendommagement%252F%26branch%3Dmaster)
- FEniCSx version:
 [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.enpc.fr%2Fnavier-fenics%2Ffenics-binder/fenicsx?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.enpc.fr%252Fjeremy.bleyer%252Fendommagement%26urlpath%3Dlab%252Ftree%252Fendommagement%252F%26branch%3Ddolfinx)
